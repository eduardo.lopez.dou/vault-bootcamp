# Cryptography Overview​

This lecture will provide students with a general landscape about cryptography.

# Contents

1. Overview​
1. Hashes​
1. Block ciphers​
1. Public key​
1. Digital Signatures​
1. Certificates​
1. Main protocols
1. Web apps

# Labs

## Check version
```bash
openssl version
openssl version -a
```

## List options
```bash
openssl help
```

## Benchmark
```bash
openssl speed
```

## Generate rand data
```bash
openssl rand -hex 20
openssl rand -base64 20
```

## Create hash
```bash
openssl dgst -h
openssl dgst -sha1 test.vol
```

## hmac
```bash
openssl dgst -sha1 -hmac "123" test.vol
```

## symmetric cipher
```bash
# list ciphers
openssl list-cipher-commands
# encrypt data
openssl enc -e -aes-128-cbc -in test.vol -out test.vol.enc
# decrypt data
openssl enc -d -aes-128-cbc -in test.vol.enc -out test.vol.dec
# check the integrity
sha1sum test.vol*
```

## rsa
```bash
# create private key
openssl genrsa -out rsa.key 2048
# print key information
openssl rsa -in rsa.key -text -noout
# create public key
openssl rsa -in rsa.key -pubout -out rsa.pub
# create data file
echo "1234567890123456" > password.txt
# encrypt data with public key
openssl rsautl -encrypt -pubin -inkey rsa.pub  -in password.txt -out password.txt.rsa
# decrypt data with private key
openssl rsautl -decrypt -inkey rsa.key -in password.txt.rsa -out password.txt.dec
# sign data with private key (digital signatures)
openssl dgst -sha256 -sign rsa.key -out password.txt.sign password.txt
# verify data with public key (digital signatures)
openssl dgst -sha256 -verify  rsa.pub -signature password.txt.sign password.txt
```

## dsa
```bash
# create parameters for dsa
openssl dsaparam 1024 < /dev/random > dsaparam.pem
# create dsa private key
openssl gendsa dsaparam.pem -out dsa.key
# create dsa public key
openssl dsa -in dsa.key -pubout -out dsa.pub
# create hash of some file
sha1sum < password.txt | awk '{print $1}' > password.txt.sha1
# sign a file
openssl dgst -dss1 -sign dsa.key password.txt.sha1 > password.txt.sha1.sign
# verify a file
openssl dgst -dss1 -verify dsa.pub -signature  password.txt.sha1.sign password.txt.sha1
```

## ecc
```bash
# list curves
openssl ecparam -list_curves
# create ecc private key
openssl ecparam -name secp256k1 -out ecc.key
# check information
openssl ecparam -in ecc.key -text -param_enc explicit -noout
```

## x.509
```bash
# create CA authority
openssl genrsa -out ca.key 2048
openssl req -new -x509 -days 365 -key ca.key -out ca.crt
openssl x509 -in ca.crt -text -noout

# create key, csr,crt files
openssl genrsa -out server.key 2048
openssl req  -new -key server.key -out server.csr -subj  "/C=LI/ST=Liesland/L=Lies/O=Test/OU=Server/CN=localhost"
openssl x509 -req  -days 365 -in server.csr -CA ca.crt -CAkey ca.key -set_serial 01 -out server.crt
openssl x509 -in server.crt -text -noout

# verify certs
openssl verify -CAfile ca.crt server.crt 
openssl s_server -accept 443 -cert server.crt -key server.key 
openssl s_client -connect localhost:443 -CAfile  ca.crt
```

# Challenge​

1. Create an encrypted storage and install Apache DocumentRoot on it.
1. Setup Apache server con TLS 1.3 with certificates based on elliptic curves.​
1. Install a NGINX server, configure a reverse proxy for Apache and enable mutal TLS (elliptic curves) between client and NGINX.​
1. Install and configure a TOR hidden service for NGINX.

# Links

* [Veracrypt](https://www.veracrypt.fr/en/Home.html)
* [Tor](https://community.torproject.org/onion-services/setup/)
* [Apache Config](https://www.digitalocean.com/community/tutorials/how-to-configure-the-apache-web-server-on-an-ubuntu-or-debian-vps)
* [Apache TLS](https://www.digitalocean.com/community/tutorials/how-to-secure-apache-with-let-s-encrypt-on-ubuntu-20-04-es) 
* [Nginx reverse proxy](https://www.digitalocean.com/community/tutorials/como-configurar-nginx-como-servidor-web-y-proxy-inverso-para-apache-en-un-servidor-ubuntu-18-04-es) 

# Appendix
* [Cryptography Course](https://www.youtube.com/watch?v=2aHkqB2-46k&list=PL6N5qY2nvvJE8X75VkXglSrVhLv1tVcfy)
* [Book](https://www.crypto-textbook.com/)
* [Overview](http://ksuweb.kennesaw.edu/~she4/2018Fall/cs4322/Slides/13Cryptography.pdf)
* [the world tomorrow - The Julian Assange Show: Cypherpunks, Part 1 ](https://www.youtube.com/watch?v=eil_1j72LOA&list=PL19A6F6A10DCFB253&index=8)
* [the world tomorrow - The Julian Assange Show: Cypherpunks, Part 2 ](https://www.youtube.com/watch?v=6DQghUChYtk&list=PL19A6F6A10DCFB253&index=9)
* [Homomorphic Encryption](https://homomorphicencryption.org/introduction/)
* [Rise of the Machines: A Cybernetic History by Thomas Rid - Chapter Anarchy](https://www.pdfdrive.com/rise-of-the-machines-a-cybernetic-history-e194568214.html)
* [Password Hashing Competition](https://www.password-hashing.net/)
* [Algorithms Reference](https://coherence.3vidence.com/)
* [Cypherpunk's Calendar](https://github.com/liesware/cypherpunksdays)
* [A Cypherpunk's Manifesto](https://www.activism.net/cypherpunk/manifesto.html)
* [THE CYPHERNOMICON](http://groups.csail.mit.edu/mac/classes/6.805/articles/crypto/cypherpunks/cyphernomicon/CP-FAQ)
* [Cicada3301](https://uncovering-cicada.fandom.com/wiki/Uncovering_Cicada_Wiki)
* [Coherence](https://github.com/liesware/coherence)
* [3vidence](https://www.3vidence.com/)
* [Company: Thales](https://cpl.thalesgroup.com/encryption/hardware-security-modules)
* [Company: Enveil](https://www.enveil.com/)
* [Company: Duality Tech](https://dualitytech.com/)
* [Company: Entrust](https://www.entrust.com/)


