# HashiCorp Vault Overview ​

This lecture will provide students with a general introduction to HashiCorp Vault.

# Contents

1. [What is Vault?](https://www.vaultproject.io/docs/what-is-vault)
1. [Benefits and Use Cases of Vault​](https://www.vaultproject.io/docs/use-cases)
1. [Comparing Versions of Vault](https://www.hashicorp.com/products/vault/features)

# Labs
* [First Secret](https://learn.hashicorp.com/tutorials/vault/getting-started-first-secret?in=vault/interactive)
* [Getting started UI​](https://learn.hashicorp.com/tutorials/vault/getting-started-install?in=vault/getting-started-ui)
* [Install](https://learn.hashicorp.com/tutorials/vault/getting-started-install?in=vault/getting-started-ui) 

# Challenge​
1. Find 10 clients successful histories​
1. Find how much a data breach costs​
1. Find de biggest data breaches

# Appendix
* [CLI](https://www.vaultproject.io/docs/commands)
* [Dev Server](https://www.vaultproject.io/docs/concepts/dev-server)
* [UI](https://www.vaultproject.io/docs/configuration/ui)
* [API](https://www.vaultproject.io/api) 
* [Identity](https://www.hashicorp.com/identity-based-security-and-low-trust-networks)
* [Integration](https://www.vaultproject.io/docs/partnerships)
