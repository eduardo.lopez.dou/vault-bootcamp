# Secrets Engines​

This lecture the students understand vault secret engines and how to deploy them.

# Contents
1. [Overview](https://www.vaultproject.io/docs/secrets)​
1. Dynamic secrets vs. static secrets
1. [KV​](https://www.vaultproject.io/docs/secrets/kv/kv-v2)​
1. [Cubbyhole​](https://www.vaultproject.io/docs/secrets/cubbyhole)​
1. [PKI​](https://www.vaultproject.io/docs/secrets/pki)​
1. [SSH](https://www.vaultproject.io/docs/secrets/ssh)

# Labs
1. [KV](https://learn.hashicorp.com/tutorials/vault/getting-started-secrets-engines)
1. [Versioned Key/Value Secrets Engine](https://learn.hashicorp.com/tutorials/vault/versioned-kv?in=vault/interactive) (CLI,API,UI)
1. [Manage Secrets Engines](https://learn.hashicorp.com/tutorials/vault/getting-started-secrets-ui?in=vault/getting-started-ui)
1. [Static Secrets: Key/Value Secrets Engine](https://learn.hashicorp.com/tutorials/vault/static-secrets)
1. [Cubbyhole Response Wrapping](https://learn.hashicorp.com/tutorials/vault/cubbyhole-response-wrapping?in=vault/interactive)
1. [Dynamic Secrets: Database Secrets Engine](https://learn.hashicorp.com/tutorials/vault/database-secrets?in=vault/interactive)
1. [Build Your Own Certificate Authority (CA)](https://learn.hashicorp.com/tutorials/vault/pki-engine?in=vault/interactive)

# Challenge
1. Configure TOTP secret engine​
1. Create a web app  compatible with Authy or similar apps , which uses Vault TOTP​
1. Configure SSH secret engine
