# Policies​

This lecture the students understand why policies are important in Vault and how they are used to secure an environment.

# Contents
1. [Overview](https://www.vaultproject.io/docs/concepts/policies)

# Labs
1. [Getting started](https://learn.hashicorp.com/tutorials/vault/getting-started-policies?in=vault/interactive)
1. [Policies](https://learn.hashicorp.com/tutorials/vault/policies)
1. [Policies UI](https://learn.hashicorp.com/tutorials/vault/getting-started-policies-ui)
1. [Policies API](https://learn.hashicorp.com/tutorials/vault/write-a-policy-using-api-docs?in=vault/policies)
1. [Policy templating](https://learn.hashicorp.com/tutorials/vault/policy-templating?in=vault/policies)
