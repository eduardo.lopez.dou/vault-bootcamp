# Tokens

This lecture the students understand the different kinds of tokens and their properties.

# Contents
1. [Overview](https://www.vaultproject.io/docs/concepts/tokens)

# Labs
1. [Tokens](https://learn.hashicorp.com/tutorials/vault/tokens?in=vault/tokens)
1. [Batch Tokens](https://learn.hashicorp.com/tutorials/vault/batch-tokens?in=vault/tokens)
1. [Token Management](https://learn.hashicorp.com/tutorials/vault/token-management?in=vault/tokens)
