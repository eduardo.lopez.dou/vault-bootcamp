# Encryption as a service

This lecture the students understand transit engine and how to deploy it.

# Contents
1. [Overview](https://www.vaultproject.io/docs/secrets/transit)

# Labs
1. [Transit Secrets Engine](https://learn.hashicorp.com/tutorials/vault/eaas-transit?in=vault/encryption-as-a-service)
1. [Transit Secrets Re-wrapping](https://learn.hashicorp.com/tutorials/vault/eaas-transit-rewrap?in=vault/encryption-as-a-service)

# Challenge

1. Configure Transit Engine
1. Create a web app to encrypt and decrypt data using Transit Engine

[Tip](https://learn.hashicorp.com/tutorials/vault/eaas-spring-demo?in=vault/encryption-as-a-service)
