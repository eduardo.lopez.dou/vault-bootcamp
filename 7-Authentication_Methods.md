# Authentication Methods ​

This lecture the students understand auth methods and how to deploy them.

# Contents
1. [Overview](https://www.vaultproject.io/docs/concepts/auth)

# Labs
1. [Userpass - UI](https://learn.hashicorp.com/tutorials/vault/getting-started-auth-ui)
1. [Token/Github](https://learn.hashicorp.com/tutorials/vault/getting-started-authentication?in=vault/getting-started)
1. [API](https://learn.hashicorp.com/tutorials/vault/getting-started-apis?in=vault/getting-started)
1. [Approle](https://learn.hashicorp.com/tutorials/vault/approle?in=vault/auth-methods)
1. [Approle - best practices](https://learn.hashicorp.com/tutorials/vault/approle-best-practices?in=vault/auth-methods)
1. [Agent](https://learn.hashicorp.com/tutorials/vault/agent-read-secrets?in=vault/app-integration)
1. [Entities and Groups](https://learn.hashicorp.com/tutorials/vault/identity?in=vault/auth-methods)
1. [Ldap - secrete engine](https://learn.hashicorp.com/tutorials/vault/openldap?in=vault/secrets-management)
1. [Username Templating](https://learn.hashicorp.com/tutorials/vault/username-templating?in=vault/secrets-management)
1. [SSH - secret engine](https://learn.hashicorp.com/tutorials/vault/ssh-otp?in=vault/secrets-management)
1. [Java App demo](https://learn.hashicorp.com/tutorials/vault/eaas-spring-demo?in=vault/app-integration)


# Challenge

1. Configure Radius Auth​
1. Configure Kerberos Auth​
1. Configure  Vault to work with AuthO​

