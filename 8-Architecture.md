# Authentication Methods ​

This lecture will walk students through the basic architecture of Vault.

# Contents
1. [Overview](https://www.vaultproject.io/docs/internals/architecture)
1. [Reference Architecture](https://learn.hashicorp.com/tutorials/vault/reference-architecture)

# Labs
1. [Auto-unseal](https://learn.hashicorp.com/tutorials/vault/autounseal-transit?in=vault/auto-unseal)

# Challenge


