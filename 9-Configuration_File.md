# Authentication Methods ​

This lecture will walk students through the basic architecture of Vault.

# Contents
1. [Overview](https://www.vaultproject.io/docs/configuration)

# Labs
1. [Deploy](https://learn.hashicorp.com/tutorials/vault/getting-started-deploy)
1. [Raft](https://learn.hashicorp.com/tutorials/vault/raft-storage)
1. [Regenerate Root](https://learn.hashicorp.com/tutorials/vault/generate-root)
1. [Rekey](https://learn.hashicorp.com/tutorials/vault/rekeying-and-rotating)
1. [Agent Caching](https://learn.hashicorp.com/tutorials/vault/agent-caching)
1. [Agent Templates](https://learn.hashicorp.com/tutorials/vault/agent-templates?in=vault/app-integration)
1. [Agent](https://learn.hashicorp.com/tutorials/vault/agent-read-secrets?in=vault/app-integration)


# Challenge
1. Configure auto unseal
1. Configure Dynamic Secrets for Database credentials with a max TTL of 10s.
1. Create a web app , which use the previous point with vault agent.
1. Renew revoke leases
