# Vault Bootcamp

# Abstract​

Vault is a tool for securely accessing secrets. A secret is anything that you want to tightly control access to, such as API keys, passwords, or 
certificates. Vault provides a unified interface to any secret, while providing tight access control and recording a detailed audit log. This course 
covers the study guide "HashiCorp Certified: Vault Associate".​


# Who this course is for:​

As HashiCorp Vault continues to grow exponentially in the market, so do the skillsets needed to properly deploy and maintain the solution. ​
The Vault Associate certification is for Cloud Engineers specializing in security, development, or operations who know the basic concepts, skills, and 
use cases associated with open source HashiCorp Vault.


# Objetives

1. Compare authentication methods​
1. Create Vault policies​
1. Assess Vault tokens​
1. Manage Vault leases​
1. Compare and configure Vault secrets engines​
1. Utilize Vault CLI​
1. Utilize Vault UI​
1. Be aware of the Vault API​
1. Explain Vault architecture​
1. Explain encryption as a service​

# Requirements​

- Basic or intermediate IT knowledge​
- Security background optional​
- Basic terminal experience with Linux​

# Resume

- Cryptography overview​
- HashiCorp Vault Overview​
- Secret Engines ​
- Policies ​
- Tokens​
- Encryption as a service ​
- Authentication Methods ​
- Architecture ​
- Configuration File ​
- Clustering and replication ​
- Enterprise ​
- Course review


Duration: 42 hrs​ \
Labs: 24 hrs ​\
Theory: 12 hrs​ \
Demos: 5 hrs​ \
Challenges: ? 

# Guide
[Study Guide - Vault Associate Certification](https://learn.hashicorp.com/tutorials/vault/associate-study)

# Schedule

##### Kickoff 
Date: 20/09/21 \
Time: 17:30 hrs \
Duration: 30 minutes

##### Cryptography Overview ​ 
Date: 21/09/21 \
Time: 17:00 hrs \
Duration: 2 hrs

##### HashiCorp Vault Overview​
Date: 23/09/21 \
Time: 17:00 hrs \
Duration: 2 hrs

##### Secret Engines​ - part 1
Date: 28/09/21 \
Time: 17:00 hrs \
Duration: 2 hrs

##### Secret Engines​ - part 2
Date: 30/09/21 \
Time: 17:00 hrs \
Duration: 2 hrs

##### Secret Engines​ - part 3
Date: 5/10/21 \
Time: 17:00 hrs \
Duration: 2 hrs

##### Policies​ - part 1
Date: 7/10/21 \
Time: 17:00 hrs \
Duration: 2 hrs

##### Policies​ - part 2
Date: 12/10/21 \
Time: 17:00 hrs \
Duration: 2 hrs

##### Tokens
Date: 14/10/21 \
Time: 17:00 hrs \
Duration: 2 hrs

##### Encryption as a service​
Date: 19/10/21 \
Time: 17:00 hrs \
Duration: 2 hrs

##### Authentication Methods ​- part 1
Date: 21/10/21 \
Time: 17:00 hrs \
Duration: 2 hrs

##### Authentication Methods ​- part 2
Date: 26/10/21 \
Time: 17:00 hrs \
Duration: 2 hrs

##### Architecture​
Date: 28/10/21 \
Time: 17:00 hrs \
Duration: 2 hrs

##### Configuration File​ - part 1
Date: 02/11/21 \
Time: 17:00 hrs \
Duration: 2 hrs

##### Configuration File​ - part 2
Date: 04/11/21 \
Time: 17:00 hrs \
Duration: 2 hrs

##### Configuration File​ - part 3
Date: 09/11/21 \
Time: 17:00 hrs \
Duration: 2 hrs

##### Enterprise
Date: 11/11/21 \
Time: 17:00 hrs \
Duration: 2 hrs

##### Clustering and replication​
Date: 16/11/21 \
Time: 17:00 hrs \
Duration: 2 hrs

##### Course review​
Date: 18/11/21 \
Time: 17:00 hrs \
Duration: 2 hrs
